﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_gsb
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void saisirNouveauVisiteurMalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSaisieVisiteur ofrm = new frmSaisieVisiteur();
            ofrm.ShowDialog();
            //ofrm.Show();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listeDesVisiteursDeLaSociétéToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmlisteVisiteur ofrm = new frmlisteVisiteur();
            ofrm.ShowDialog();
        }
    }
}
