﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_gsb.mesClasses;
using TP_gsb.mesClasses.outils;
using MySql.Data.MySqlClient;

namespace TP_gsb
{
    public partial class frmlisteVisiteur : Form
    {
        //Déclaration de la variable objet DataGridView initialisée à null
        DataGridView dglisteVisiteur = null;
        DataSet ods = null;
        public frmlisteVisiteur()
        {
            InitializeComponent();

            this.MinimumSize = new System.Drawing.Size(1210, 745);
           
            
            // appel de la méthode procédure d'initialisation du dataGrigView
            dginit();
            this.lbtitre.Location = new System.Drawing.Point(89 + this.dglisteVisiteur.Width / 2 - this.lbtitre.Width /2, 100);
            
           
        }

        /// <summary>
        /// 
        /// </summary>
        private void dginit()
        {
            this.dglisteVisiteur = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dglisteVisiteur)).BeginInit();
            //this.SuspendLayout();
            // 
            // dglisteVisiteur
            // 
            this.dglisteVisiteur.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dglisteVisiteur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dglisteVisiteur.Location = new System.Drawing.Point(89, 145);
            this.dglisteVisiteur.Name = "dglisteVisiteur";
            this.dglisteVisiteur.RowTemplate.Height = 24;
            this.dglisteVisiteur.Size = new System.Drawing.Size(960, 384);
            this.dglisteVisiteur.TabIndex = 0;

            this.dglisteVisiteur.MaximumSize = new System.Drawing.Size(960, 384);

            this.Controls.Add(this.dglisteVisiteur);

            ((System.ComponentModel.ISupportInitialize)(this.dglisteVisiteur)).EndInit();

            // Liaison evenement cellclick a selectionEnregDg
            this.dglisteVisiteur.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.selectionEnregDg);

            //positionnement graphique des boutons relativement au datagrid
            this.btnInsertDg.Location = new System.Drawing.Point(89, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnsuppVisiteur.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnupdateVisiteur.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10 + btnsuppVisiteur.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnquitter.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10 + btnsuppVisiteur.Width + 10 + btnupdateVisiteur.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            rafraichirAffichageDg();
        }

        private void rafraichirAffichageDg()
        {
            Cvisiteurs ovisiteurs = new mesClasses.Cvisiteurs();
            // appel de la méthode getdsVisiteur de l'objet de contrôle qui renvoie un objet de type DataSet
            ods = ovisiteurs.getdsVisiteur();
            //On pourrait passer par la collection de visiteurs de l'objet de contrôle mais dans ce cas le DG est en lecture seule ce qui n'est pas le but ici
            //Dictionary<string, Cvisiteur> ocoll = ovisiteurs.ocollDicovisit;
            // Affectation de l'objet DataSet comme source de données du dataGridview
            dglisteVisiteur.DataSource = ods.Tables["dgVisiteur"].DefaultView;
            //dglisteVisiteur.DataSource = ocoll.Values.ToList<Cvisiteur>();
            dglisteVisiteur.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
            dglisteVisiteur.Refresh();

        }

        private void btnInsertDg_Click(object sender, EventArgs e)
        {
            //int nbEnregInDs = this.ods.Tables[0].Rows.Count;
            int nbEnreg = dglisteVisiteur.Rows.Count; // cf rowCount
            DataGridViewRow orowDg = dglisteVisiteur.Rows[nbEnreg -2];
            bool dejaException = false;
            string query = null;

            try
            {
                string id = (string)orowDg.Cells[0].Value;
                string nom = (string)orowDg.Cells[1].Value;
                string prenom = (string)orowDg.Cells[2].Value;
                string login = (string)orowDg.Cells[3].Value;
                string mdp = (string)orowDg.Cells[4].Value;
                string adresse = (string)orowDg.Cells[5].Value;
                string cp = (string)orowDg.Cells[6].Value;
                string ville = (string)orowDg.Cells[7].Value;
                DateTime odateEmbauche = Convert.ToDateTime(orowDg.Cells[8].Value);
                string dateEmbaucheMySql = CtraitementDate.getDateFormatMysql(odateEmbauche);

                query = "insert into visiteur(id,nom,prenom,login,mdp,adresse,cp,ville,dateEmbauche) values('" + id.Trim() + "','" + nom.Trim() + "','" +
                   prenom.Trim() + "','" + login.Trim() + "','" + mdp.Trim() + "','" + adresse.Trim() + "','" + cp.Trim() + "','" +
                   ville.Trim() + "','" + dateEmbaucheMySql + "')";
            }
            catch(Exception ex)
            {
                dejaException = true;
                MessageBox.Show(ex.Message + " Veuillez saisir tous les champs ! Merci.");

            }


            if (!dejaException) // Si il n'y a pas eu de première exception
            {
                Cdao odao = new Cdao();
                try //remontée d'exception
                {
                    odao.insertEnreg(query);

                    DialogResult result = MessageBox.Show("Enregistrement bien effectué. Merci !");
                    // appel methode rafraichissement du datagrid
                    /* 
                     * demande à la classe de contrôle de fournir un dataset
                     * affecte le dataset comme source de données au datagridView
                     * rafraichit l'affichage du dataGridView
                     * */
                    rafraichirAffichageDg();

                }
                catch (MySqlException ex)
                {
                    MessageBox.Show("Il faut saisir un nouvel enregistrement. Merci !" + "\r\n" + ex.Message,"Avertissement",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                }
            }
        
        }


        string idaSupprimer = null;
        DataGridViewRow orowDg = null;
        private void selectionEnregDg(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int numRow = e.RowIndex;
                // afin de ne pas faire le traitement quand les en-têtes de colonne sont sélectionnées
                if (numRow != -1) 
                {

                    DataGridView odg = (DataGridView)sender;
                    orowDg = odg.Rows[numRow];

                    //pour éviter exception quand on sélectionne le row vide pour saisir un nouveau visiteur
                    if (!(orowDg.Cells[0].Value is System.DBNull))
                    {
                        idaSupprimer = (string)orowDg.Cells[0].Value;
                    }
                }

            }
            catch
            {
                MessageBox.Show("Vous ne pouvez pas selectionner plus d'un enregistrement ! Merci.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnsuppVisiteur_Click(object sender, EventArgs e)
        {
            if(idaSupprimer != null)
            {
                DialogResult Dlgresult = MessageBox.Show("Etes-vous sûr de vouloir supprimer l'enregistrement n° " + idaSupprimer + " ?", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dlgresult == DialogResult.Yes)
                {
                    try
                    {
                        Cdao odao = new Cdao();
                        odao.deleteEnreg("delete from visiteur where id='" + idaSupprimer + "'");
                        rafraichirAffichageDg();
                        idaSupprimer = null;
                        MessageBox.Show("Enregistrement supprimé !","Information",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    }
                    catch(MySqlException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un enregistrement à supprimer en cliquant dans la marge de couleur blanche de l'enregistrement choisi ! Merci.","Avertissement",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void btnupdateVisiteur_Click(object sender, EventArgs e)
        {
            if (idaSupprimer != null)
            {
                string idaupdate = idaSupprimer;
                DialogResult Dlgresult = MessageBox.Show("Etes-vous sûr de vouloir mettre à jour l'enregistrement n° " + idaSupprimer + " ?", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Dlgresult == DialogResult.Yes)
                {
                    try
                    {
                        string id = (string)orowDg.Cells[0].Value;
                        string nom = (string)orowDg.Cells[1].Value;
                        string prenom = (string)orowDg.Cells[2].Value;
                        string login = (string)orowDg.Cells[3].Value;
                        string mdp = (string)orowDg.Cells[4].Value;
                        string adresse = (string)orowDg.Cells[5].Value;
                        string cp = (string)orowDg.Cells[6].Value;
                        string ville = (string)orowDg.Cells[7].Value;
                        DateTime odateEmbauche = Convert.ToDateTime(orowDg.Cells[8].Value);
                        string dateEmbaucheMySql = CtraitementDate.getDateFormatMysql(odateEmbauche);

                        //je mets tout à jour c'est plus simple
                        string query = "update visiteur " +
                                       "set id='" + id.Trim() + "'," +
                                            "nom='" + nom.Trim() + "'," +
                                            "prenom='" + prenom.Trim() + "'," +
                                            "login='" + login.Trim() + "'," +
                                            "mdp='" + mdp.Trim() + "'," +
                                            "adresse='" + adresse.Trim() + "'," +
                                            "cp='" + cp.Trim() + "'," +
                                            "ville='" + ville.Trim() + "'," +
                                            "dateEmbauche='" + dateEmbaucheMySql + "' where id='" + idaupdate + "'";

                        Cdao odao = new Cdao();
 
                        odao.updateEnreg(query);

                        rafraichirAffichageDg();
                        //Remise à null des variables objets
                        idaSupprimer = null;
                        orowDg = null;
                        MessageBox.Show("Enregistrement mis à jour !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    catch (MySqlException ex)
                    {
                        MessageBox.Show(ex.Message, "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un enregistrement à mettre à jour en cliquant dans la marge de couleur blanche de l'enregistrement choisi." + "\r\n" + "Puis modifier les cellules à mettre à jour. Merci.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnquitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
