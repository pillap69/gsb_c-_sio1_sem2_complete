﻿namespace TP_gsb
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.visiteurMédicauxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.comptableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saisirNouveauVisiteurMalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visiteurMédicauxToolStripMenuItem,
            this.comptableToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(510, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // visiteurMédicauxToolStripMenuItem
            // 
            this.visiteurMédicauxToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitterToolStripMenuItem1});
            this.visiteurMédicauxToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.visiteurMédicauxToolStripMenuItem.Name = "visiteurMédicauxToolStripMenuItem";
            this.visiteurMédicauxToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.visiteurMédicauxToolStripMenuItem.Text = "Visiteurs médicaux";
            // 
            // quitterToolStripMenuItem1
            // 
            this.quitterToolStripMenuItem1.Name = "quitterToolStripMenuItem1";
            this.quitterToolStripMenuItem1.Size = new System.Drawing.Size(130, 26);
            this.quitterToolStripMenuItem1.Text = "Quitter";
            this.quitterToolStripMenuItem1.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // comptableToolStripMenuItem
            // 
            this.comptableToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitterToolStripMenuItem2});
            this.comptableToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.comptableToolStripMenuItem.Name = "comptableToolStripMenuItem";
            this.comptableToolStripMenuItem.Size = new System.Drawing.Size(95, 24);
            this.comptableToolStripMenuItem.Text = "Comptable";
            // 
            // quitterToolStripMenuItem2
            // 
            this.quitterToolStripMenuItem2.Name = "quitterToolStripMenuItem2";
            this.quitterToolStripMenuItem2.Size = new System.Drawing.Size(130, 26);
            this.quitterToolStripMenuItem2.Text = "Quitter";
            this.quitterToolStripMenuItem2.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saisirNouveauVisiteurMalToolStripMenuItem,
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(119, 24);
            this.toolStripMenuItem1.Text = "Administration";
            // 
            // saisirNouveauVisiteurMalToolStripMenuItem
            // 
            this.saisirNouveauVisiteurMalToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.saisirNouveauVisiteurMalToolStripMenuItem.Name = "saisirNouveauVisiteurMalToolStripMenuItem";
            this.saisirNouveauVisiteurMalToolStripMenuItem.Size = new System.Drawing.Size(292, 26);
            this.saisirNouveauVisiteurMalToolStripMenuItem.Text = "Saisir Nouveau Visiteur Médical";
            this.saisirNouveauVisiteurMalToolStripMenuItem.Click += new System.EventHandler(this.saisirNouveauVisiteurMalToolStripMenuItem_Click);
            // 
            // listeDesVisiteursDeLaSociétéToolStripMenuItem
            // 
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Name = "listeDesVisiteursDeLaSociétéToolStripMenuItem";
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Size = new System.Drawing.Size(292, 26);
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Text = "Liste des visiteurs de la société ";
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Click += new System.EventHandler(this.listeDesVisiteursDeLaSociétéToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.quitterToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(292, 26);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 390);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "GSB";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saisirNouveauVisiteurMalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visiteurMédicauxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comptableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem listeDesVisiteursDeLaSociétéToolStripMenuItem;
    }
}

