﻿namespace TP_gsb
{
    partial class frmlisteVisiteur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbtitre = new System.Windows.Forms.Label();
            this.btnInsertDg = new System.Windows.Forms.Button();
            this.btnsuppVisiteur = new System.Windows.Forms.Button();
            this.btnupdateVisiteur = new System.Windows.Forms.Button();
            this.btnquitter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbtitre
            // 
            this.lbtitre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbtitre.AutoSize = true;
            this.lbtitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtitre.ForeColor = System.Drawing.Color.DimGray;
            this.lbtitre.Location = new System.Drawing.Point(94, 67);
            this.lbtitre.Name = "lbtitre";
            this.lbtitre.Size = new System.Drawing.Size(363, 25);
            this.lbtitre.TabIndex = 2;
            this.lbtitre.Text = "LISTE DES VISITEURS MEDICAUX";
            // 
            // btnInsertDg
            // 
            this.btnInsertDg.BackColor = System.Drawing.Color.Ivory;
            this.btnInsertDg.Location = new System.Drawing.Point(19, 351);
            this.btnInsertDg.Name = "btnInsertDg";
            this.btnInsertDg.Size = new System.Drawing.Size(149, 41);
            this.btnInsertDg.TabIndex = 1;
            this.btnInsertDg.Text = "Ajouter un visiteur";
            this.btnInsertDg.UseVisualStyleBackColor = false;
            this.btnInsertDg.Click += new System.EventHandler(this.btnInsertDg_Click);
            // 
            // btnsuppVisiteur
            // 
            this.btnsuppVisiteur.BackColor = System.Drawing.Color.Ivory;
            this.btnsuppVisiteur.Location = new System.Drawing.Point(193, 351);
            this.btnsuppVisiteur.Name = "btnsuppVisiteur";
            this.btnsuppVisiteur.Size = new System.Drawing.Size(139, 40);
            this.btnsuppVisiteur.TabIndex = 3;
            this.btnsuppVisiteur.Text = "Supprimer visiteur";
            this.btnsuppVisiteur.UseVisualStyleBackColor = false;
            this.btnsuppVisiteur.Click += new System.EventHandler(this.btnsuppVisiteur_Click);
            // 
            // btnupdateVisiteur
            // 
            this.btnupdateVisiteur.BackColor = System.Drawing.Color.Ivory;
            this.btnupdateVisiteur.Location = new System.Drawing.Point(353, 351);
            this.btnupdateVisiteur.Name = "btnupdateVisiteur";
            this.btnupdateVisiteur.Size = new System.Drawing.Size(227, 40);
            this.btnupdateVisiteur.TabIndex = 4;
            this.btnupdateVisiteur.Text = "Mise à jour données visiteur";
            this.btnupdateVisiteur.UseVisualStyleBackColor = false;
            this.btnupdateVisiteur.Click += new System.EventHandler(this.btnupdateVisiteur_Click);
            // 
            // btnquitter
            // 
            this.btnquitter.BackColor = System.Drawing.Color.Ivory;
            this.btnquitter.Location = new System.Drawing.Point(612, 351);
            this.btnquitter.Name = "btnquitter";
            this.btnquitter.Size = new System.Drawing.Size(132, 40);
            this.btnquitter.TabIndex = 5;
            this.btnquitter.Text = "Quitter";
            this.btnquitter.UseVisualStyleBackColor = false;
            this.btnquitter.Click += new System.EventHandler(this.btnquitter_Click);
            // 
            // frmlisteVisiteur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 406);
            this.Controls.Add(this.btnquitter);
            this.Controls.Add(this.btnupdateVisiteur);
            this.Controls.Add(this.btnsuppVisiteur);
            this.Controls.Add(this.btnInsertDg);
            this.Controls.Add(this.lbtitre);
            this.Name = "frmlisteVisiteur";
            this.Text = "Liste des visiteurs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbtitre;
        private System.Windows.Forms.Button btnInsertDg;
        private System.Windows.Forms.Button btnsuppVisiteur;
        private System.Windows.Forms.Button btnupdateVisiteur;
        private System.Windows.Forms.Button btnquitter;
    }
}